define henks_ircservices::network (
  String $run_user = "irc-services-${name}",
  String $network_name = $name,
) {
  $service_base = "/etc/s6-services/irc-services-${network_name}/"

  user {
    $run_user:
      ensure     => present,
      groups     => [ 'ssl-cert', ],
      password   => '!',
      managehome => true,
    ;
  }

  file {
    $service_base:
      ensure => directory,
    ;
    "${service_base}/run":
      ensure  => present,
      content => epp(
        'henks_ircservices/etc/s6-services/irc-services/run.epp',
        {
          'network_name' => $network_name,
          'run_user'     => $run_user,
        },
      ),
      mode    => '0755',
    ;
    "${service_base}/timeout-kill":
      ensure  => present,
      content => '0',
    ;
    "${service_base}/log/":
      ensure => directory,
    ;
    "${service_base}/log/run":
      ensure  => present,
      content => epp(
        'henks_ircservices/etc/s6-services/irc-services/log/run.epp',
        {
          'network_name' => $network_name,
        },
      ),
      mode    => '0755',
    ;
    "/var/log/s6/irc-services-${network_name}/":
      ensure => directory,
      mode   => '0700',
    ;
    "/var/log/s6/irc-services-${network_name}/logs/":
      ensure => directory,
      mode   => '0700',
    ;
  }

  service {
    "irc-services-${network_name}":
      ensure   => running,
      enable   => true,
      provider => 's6',
    ;
  }
}
