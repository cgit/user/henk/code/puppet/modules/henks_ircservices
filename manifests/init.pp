class henks_ircservices (
  Array $networks
) {
  require 'sslcert'
  require 'profile::pkg_collections::buildenv'

  $networks.each |$network| {
    henks_ircservices::network {
      $network['name']:
        run_user => $network.dig('system_user').lest | | { "irc-services-${network['name']}" },
      ;
    }
  }
}
